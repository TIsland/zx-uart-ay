; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

;
    MODULE CMDLINE
cmd_line_flg_next:  ; skip whitespace after parsing a flag
    ; HL -- current pointer
    ld a, (hl)
    cp ' '  ; TODO: what if there is more than one?
    jr nz, is_end_of_cmd_line
    inc hl  ; whitespace, skip to the next char
    scf
    ret     ; CF=1 -- more chars left
is_end_of_cmd_line:
    ; A -- current character
    ; the data belongs to BASIC, thus:
    cp 0    ; null-terminated
    jr z, .end
    cp 0x0d ; EOL-terminated
    jr z, .end
    cp ':'  ; : BASIC statement separator terminated
    jr z, .end
    scf
.end:       ; ZF=0 means CF=0
    ret     ; CF=0 -- end of cmd line, CF=1 -- more chars left

uppercase:
    cp 'a'
    jr c, .done
    cp 'z'+1
    jr nc, .done
    and %11011111    ; uppercase
.done
    ret

PARSE:
    ; HL -- points to the ESXDOS cmd line
    ; DE -- ouput buffer for anything that's not an option
    ; A NOT preserved, BC NOT preserved
    ; NOTE: DE will NOT be null terminated, add null there AFTER this!
    ; try to keep A, HL, DE preserved. IX points to the settings block
    ld a, (hl)
    call is_end_of_cmd_line
    ret nc  ; CF=1 - more chars left
    inc hl  ; advance to the next char before checking anything
    cp '-'
    jr z, .option
    ld (de), a  ; not a flag, just copy to the "input data" area
    inc de
    jr PARSE
.option:
    ld a, (hl)  ; we know that A='-', HL advanced to the next char, load it
    call is_end_of_cmd_line
    ret nc  ; CF=1 - more chars left
    push hl ; save CMD-line pointer, we need HL
    call uppercase
    ld hl, OPTIONS + OPTIONS_LEN - 1    ; last entry in OPTIONS
    ld bc, OPTIONS_LEN
    cpdr        ; find A in OPTIONS
    jp po, .notfound    ; PV=0, BC=0, option NOT found (need leading 0 in OPTIONS)
    ; now BC - index in OPTIONS array
    ld hl, HANDLERS
    add hl, bc
    add hl, bc  ; ((word *)HANDLERS)[BC]
    ld (.handler), hl   ; SMC, preload target address
    call .redirect
.notfound:
    pop hl
    inc hl
    call cmd_line_flg_next
    ret nc
    jr PARSE
.redirect:
.handler equ $ + 1  ; SMC
    ld hl, (HANDLERS+1) ; load target jump address to HL
    jp (hl) ; NOTE: it actually does JP HL, there is NO indirection!
    ; HANDLER ends with RET and returns to opcode after 'call .redirect'

; NOTE: options must be uppercase!!!
; OPTIONS:    defb 0, 'A', 'F', 'G', 'X'  ; extra 0 needed to use PV, PV=1 (PE) - option found
; OPTIONS_LEN equ $ - OPTIONS
; HANDLERS:   defw 0, FLG_A, FLG_F, FLG_G, FLG_X
;     ASSERT OPTIONS_LEN == (($ - HANDLERS)/2)  ; make sure tables match
; code scans OPTIONS, finds offset for HANDLER and calls it

    ENDMODULE

; EOF vim: et:ai:ts=4:sw=4: