;

; TuKabel by Baze and Zilog, Pavel Vymetálek
; idea extracted from https://vym.cz/sercp/

; NOTE: there is no flow control or BREAK checks, there is not
; enough time for that. The procedure will exit only after receiving
; the expected number of bytes (passed in BC). The worst part --
; there are NO ERROR CHECKS whatsoever! If code looses sync --
; "that's all, folks".
;
; TODO: while it MAY be possible to insert timeout in WSTRTBT routine,
; receiving at 115200 is just way too taxing for old speccy it will
; only work with unstructured data if sender is making long pauses
; between each byte.

; wild guess: you may want to TRANSMIT with 2 stop bits to make sure
; WSTRTBT can easily synchronise, as not all steps take 30.79 T-states

    MODULE UART

    ; static allocation of the jump table (UNdefined) takes
    ; approx 180..255 extra bytes (depends on the final alignment)
    ; you need to define RS232_115200_JUMPTABLE pointing to the
    ; 256-bytes aligned memory area where jump table will be created
    DEFINE SW_JUMPTABLE

BAUD defw 0 ; DUMMY, for API compatibility

init_port:  ; stub to make API compatible with 9600
        ; just fall through, we don't support port selection yet
init:
    IFDEF SW_JUMPTABLE
        CALL       RS232_115200_RCV_SETUP
    ENDIF
    di

    ld	bc, 0xFFFD  ; AY register select port
    ld	a, 7        ; select AY register 7 (Mixer)
    out	(c), a      ;
    ld	a, b        ; A=FF / set IO port as output (1 on bit 6) and disable all sound channels
    ld	b, $BF      ; BC=BFFD, data port
    out	(c), a      ;

    ; ld	b, a        ; BC=FFFD / AY register port
    ; ld	a, (AYIOPRT); select AY register 14 (I/O port)
    ; out	(c), a      ;
    ; ld	a, b        ; A=FF
    ; ld	b, $BF      ; BC=BFFD / data port
    ; out	(c), a      ; Bit 2: RS232  CTS (out) - 0=Spectrum ready to receive, 1=Busy

        LD         BC,0xfffd    ; AY register select port
        LD         A,0xe        ; AY IO Port 1
        OUT        (C),A        ; select AY IO Port 1

    ei

    ret 

@AYIOPRT    equ BTBUF           ; stub to make API compatible with 9600

RS232_WR_BT:    ; POC
        push bc, de, hl
        ld hl, BTBUF
        ld (hl), a
        ld de, 1
        call ESP12.RS232_TX_ENTRY
        pop hl, de, bc
        ret

RS232_RD_BT:    ; POC
        ld de, BTBUF
        ld bc, 1
        call RS232_115200_RD_BLK
BTBUF equ $ + 1
        ld a,0
        scf
dtr:    ; no op call            ; stub to make API compatible with 9600
        ret


RS232_115200_RD_BLK:
    ; DE - data buffer to store incoming bytes
    ; BC - expected data block length
        DEC        DE   ; length - 1, RCV0BIT starts with DE comparison
        DI
        CALL       RCV_BLOCK_115200
        EI
        RET

; TL;DR: code flow is controlled by JP (HL) where L is the byte
; read from the IO Port.
RCV_BLOCK_115200
        ; DE - buffer-1, BC - expected length
        LD         L,0x80
        DEC        BC   ; dec length?
        PUSH       BC   ; save length
        LD         BC,0xfffd
        PUSH       BC   ; save port read/write address
        EXX             ; shadow regs
        POP        BC   ; port read/write address
        POP        DE   ; expected length - 1
        LD         IX,RCVBYTE
        LD         HL,JMPTABLE|0xff; see WSTRTBT
        JP         (HL) ; wait for start bit
        ; timing is tight, there is 30.79 T-states per bit (128k)
        ; HL is used for conditional jumps, code is aligned in the memory
        ; in a manner allowing the received BYTE (IO Port read yield byte
        ; even though we need only the highest bit) to direct code flow
        ; to the appropriate branch
RCVBYTE:
        IN         H,(C); 12T AY IO Port 1, bit 1
        RL         H    ;  8T high bit of H (TXD) to CF
        RRA             ;  4T CF (TDX) to low bit of A
        AND        0xf8 ;  7T %1111 1000
        ;   elapsed: 31T
        IN         H,(C); 12T AY IO Port 1, bit 2
        RL         H    ;  8T high bit of H (TXD) to CF
        RRA             ;  4T CF (TDX) to low bit of A
        AND        0xf8 ;  7T %1111 1000
        ;   elapsed: 31T
        IN         H,(C); 12T AY IO Port 1, bit 3
        RL         H    ;  8T high bit of H (TXD) to CF
        RRA             ;  4T CF (TDX) to low bit of A
        AND        0xf8 ;  7T %1111 1000
        ;   elapsed: 31T
        IN         H,(C); 12T AY IO Port 1, bit 4
        RL         H    ;  8T high bit of H (TXD) to CF
        RRA             ;  4T CF (TDX) to low bit of A
        AND        0xf8 ;  7T %1111 1000 ensure low bits are 0, this is necessary to receive bits 6 and 7
        ;   elapsed: 31T
        IN         H,(C); 12T AY IO Port 1, bit 5
        RL         H    ;  8T high bit of H (TXD) to CF
        RRA             ;  4T CF (TDX) to low bit of A
        INC        DE   ;  6T
        ;   elapsed: 30T
        IN         H,(C); 12T AY IO Port 1, bit 6
        RL         H    ;  8T high bit of H (TXD) to CF
        RRA             ;  4T CF (TDX) to low bit of A
        RRA             ;  4T shift one more time placing bits 0-6 to the right positions
        LD         H,A  ;  9T now we have bits 0-6 in H, H7=0
        ;   elapsed: 37T
        IN         A,(C); 12T AY IO Port 1, bit 7
        AND        L    ;  4T L=0x08
        OR         H    ;  4T combine 7th bit with the rest from H
        ; 8th bit, the STOP bit is completely ignored
        ; instead we store the data bits receved at (DE)
        LD         (DE),A; 7T
        EXX             ;  4T
        ; if DE=0, HL points to xx00 (pre-filled with C9/RET)
        ; otherwise HL points to xxFF "wait for START bit"
        JP         (HL) ;  4T
        ;   elapsed: 35T

; Bit 4: KEYPAD DTR (in)  - 0=Keypad ready for data,     1=Busy
; Bit 5: KEYPAD TXD (in)  - 0=Receive high bit,          1=Receive low bit
; Bit 6: RS232  DTR (in)  - 0=Device ready for data,     1=Busy
; Bit 7: RS232  TXD (in)  - 0=Receive high bit,          1=Receive low bit
;   0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
; 0000 0001 0010 0011 0100 0101 0110 0111 1000 1001 1010 1011 1100 1101 1101 1111
; TX=0 TX=0 TX=0 TX=0 TX=0 TX=0 TX=0 TX=0 TX=1 TX=1 TX=1 TX=1 TX=1 TX=1 TX=1 TX=1
; TX=1 constantly == phys low line, idle
;       START 0 1 2 3 4 5 6 7 STOP
; DATA:   1   ? ? ? ? ? ? ? ?   0
; TX      0                     1
; The following code uses clever trick to do conditional jump
; where target addres is defined by the byte read from IO Port.
; Code only expects TXD and "DTR" (RTS) lines to change,
; which yields only 16 combinations of the low address byte:
;   0f, 1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, af, bf, cf, df, ef, ff
; The necessary speed is achived by using the byte as is and not
; trying to separate and analyse the TX data bit.

; The code must be aligned to 256 byte boudary
; RS232_115200_RCV_SETUP takes care of that

    MACRO RECEIVE_0TH_BIT
; read the least significant data bit, calculate remaining length
; in the block
; to be placed at xx0f, xx1f, xx2f, xx3f, xx4f, xx5f, xx6f, xx7f
; the WSTRTBT code block passess control here once start bit is detected
        LD         A,D  ; 9T DE - expected length - 1
        OR         E    ; 4T !=0 if ANY bits are set in DE
        ADD        A,0xff;7T if any bits in D or E is set, CF=1
        SBC        A,A  ; 4T A-A-CF: DE!=0 -> 0xff, DE==0 -> 0x00
        LD         L,A  ; 9T L=0 if D==E==0, 0xff otherwise
        ; that ensures jump to xxff (below) after reading this byte
        ; IF we expect more bytes. If we do NOT expect more bytes
        ; (DE=0), it jumps to xx00, which is pre-filled with C9/RET
        ;   elapsed: 33T
        IN         A,(C);12T AY IO Port 1 READ, LSB of the incoming byte
        DEC        DE   ; 6T length
        EXX             ; 4T back to DE = buf -1, bc = len - 1, L = 0x80
        JP         (IX) ; 8T RCVBYTE
        ;   elapsed: 30T
        ; TOTAL:    63T
    ENDM

    MACRO WAITSTARTBIT
; byte rx flow starts here, waiting for the START BIT
; to be placed at xx8f, xx9f, xxaf, xxbf, xxcf, xxdf, xxef, xxff
; activated when TX=1, loops here while TX=1 (low phys),
; then switches to RCV0BIT code block to read LSBit
        IN         L,(C); 12T AY IO Port 1 READ
        ; TX=1 (low phys, idle line)  -> jump to xx[8-f]f:WSTRTBT
        ; TX=1 (high phys, START bit) -> jump to xx[0-7]f:RCV0BIT
        JP         (HL) ;  4T
        ; TOTAL:     16T
    ENDM

    IFDEF SW_JUMPTABLE
RCV0BIT:
    RECEIVE_0TH_BIT

WSTRTBT:
    WAITSTARTBIT

    ASSERT 0 == (low RS232_115200_JUMPTABLE)
JMPTABLE    equ RS232_115200_JUMPTABLE

; pre-fill memory with opcodes to handle conditional jumps
; via JP (HL) "magic"
; can be done at compile time, see IFDEFs
RS232_115200_RCV_SETUP:
        LD         A,0xc9       ; RET
        LD         (JMPTABLE),A
        LD         B,0x8
        LD         C,0x0
.T0STUP:PUSH       BC
        LD         DE,JMPTABLE|0x0f ; xx[0-7]f
        LD         A,E      ; A=0x0F
        ADD        A,C      ; A=0xF+C
        LD         E,A      ; E=E+C
        LD         HL,RCV0BIT
        LD         BC,0xc   ; 12, length of code at RCV0BIT
        LDIR       ; copy RCV0BIT to xx[0-7]f
        POP        BC       ; B=8, C=0
        DEC        B
        LD         A,B
        OR         A
        JR         Z,.T1  ; counted down B to 0?
        LD         A,C
        ADD        A,0x10
        LD         C,A      ; C=C+0x10
        JP         .T0STUP
.T1:
        LD         B,0x8
        LD         C,0x0
.T1STUP:PUSH       BC
        LD         DE,JMPTABLE|0x8f ; xx[8-f]f
        LD         A,E
        ADD        A,C
        LD         E,A      ; E=E+C
        LD         HL,WSTRTBT
        LD         BC,0x3
        LDIR       ; copy WSTRTBT to xx[8-f]f
        POP        BC
        DEC        B
        LD         A,B
        OR         A
        JR         Z,.DONE ; counted down B to 0, return
        LD         A,C
        ADD        A,0x10
        LD         C,A      ; C=C+0x10
        JP         .T1STUP  ; back to filling mem at xx[8-f]f+xx
.DONE: RET

    ELSE ; NOTdef SW_JUMPTABLE
        ALIGN 256

JMPTABLE:
        RET
        ; all the tricks with DEFS and $ to ensure code starts at xxNf
        DEFS 14
        DUP 8
        RECEIVE_0TH_BIT
        DEFS 0xf - (low $)%16   ; ALIGN works in a weird way
        EDUP

        DUP 8
        WAITSTARTBIT
        DEFS 0xf - (low $)%16   ; ALIGN works in a weird way
        EDUP
    ENDIF ; SW_JUMPTABLE

    DISPLAY "115200 BLOCK",/A,$-RS232_115200_RD_BLK
    
    ENDMODULE

; EOF vim: et:ai:ts=4:sw=4: