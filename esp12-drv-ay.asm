;
    MODULE ESP12

; https://oldcomp.cz/viewtopic.php?f=39&t=7837&start=15
; https://cygnus.speccy.cz/download/zx128k_rs232/rs232_martin1_ay_128k_transmit_sequence_115200bps_exx.html

;================================================ ================================================ 
; RS232 - transmitting through AY-3-8912 without data flow control 
; 
; 115200bps (8.68056μs) 30.78906T on ZX128, 31T will take 8.7400μs, error +0.7% (114416bps) 
; 30.38194T on ZX48k, 31T will take 8.8571μs, error +2.0% (112903bps) 
;=========================== ================================================== =====================

        ; cpu     z80undoc
        ;  org     32768

factory_reset:
    di             ; do not interrupt 
    ld bc, 0xFFFD  ; "select AY register" port
    ld a, 7        ; AY register 7 Mixer
    out (c), a 
    ld bc, 0xBFFD  ; turn off sound and set I/O port as output (bit 6 in log. 1) 
    ld a, 255
    out (c), a 
    ld bc, 0xFFFD  ; pre-select register 14 in AY 
    ld a, (AYIOPRT); ld a, 14
    out (c), a

    ld hl, RESET_SEQ        ; address from where 
    ld de, RESET_SEQ_LEN    ; number of bytes to transfer 

RS232_TX_ENTRY:
    exx
    ld bc, 0xBFFD  ; data port address to secondary BC' 
    exx
    ; NOTE: no CTS/RTS!

TRANSMIT_LOOP:
    ld a ,(hl)      ; 7T take the byte to be sent 
    exx             ; 4T secondary register set with BC ready so I don't change important HL 
    rrca            ; 4T prerotate data 4x right from bit 0 to bit 4 
    rrca            ; 4T 
    rrca            ; 4T 
    rrca            ; 4T 
    ld d, a         ; 4T and save A to D (don't touch HL' so I don't have to save it when returning to BASIC) 
; start bit 
    ld a, 11110111b ; 7T start bit 
    out (c), a      ; write 12T to the port (always log. 0 = start bit)

    rrc d           ; 8T rotate L, bit 0 at bit position 3 (LSB) 
    ld a, 11110111b ; 7T prepare mask 
    or d            ; 4T unmask, set all but significant bit to 1 
    out (c), a      ; 12T write to the port, a total of 31T from the previous out (c), a

    rrc d           ; 8T bit 1 at bit position 3 
    ld a, 11110111b ; 7T 
    or d            ; 4T mask 
    out (c), a      ; 12T write to the port
        
    rrc d           ; 8T bit 2 
    ld a, 11110111b ; 7T 
    or d            ; 4T mask 
    out (c), a      ; 12T write to the port
        
    rrc d           ; 8T bit 3 
    ld a, 11110111b ; 7T 
    or d            ; 4T mask 
    out (c), a      ; 12T write to the port
        
    rrc d           ; 8T bit 4 
    ld a, 11110111b ; 7T 
    or d            ; 4T mask 
    out (c), a      ; 12T write to the port
        
    rrc d           ; 8T bit 5 
    ld a, 11110111b ; 7T 
    or d            ; 4T mask 
    out (c), a      ; 12T write to the port
        
    rrc d           ; 8T bit 6 
    ld a, 11110111b ; 7T 
    or d            ; 4T mask 
    out (c), a      ; 12T write to the port

    rrc d           ; 8T bit 7 
    ld a, 11110111b ; 7T 
    or d            ; 4T mask 
    out (c), a      ; 12 write to port, last bit (MSB)

    rrc d           ; 8T deceleration 
    nop             ; 4T deceleration 
    ld a, 11111111b ; 7T unmask stop bit to 1 
    out (c), a      ; Write 12T to the port, the stop bit starts here

    exx             ; 4T return primary set of registers with address in HL and length in DE 
    inc hl          ; 6T next address in RAM 
    dec de          ; 6T o byte to process less 
    ld a, d         ; 4T test if all 
    or e            ; 4T 
    jp nz, TRANSMIT_LOOP ; 10T and repeat until all data is transferred

; repetition occurs with a delay of 12+4+6+6+4+4+10 = 46T 
; plus at the beginning of the cycle another 7+4+4+4+4+4+4+7 = 38T 
; a total of 84T stop bit length during sequence transmission, i.e. about 2.7 times the duration of one bit

    ei
    ret

RESET_SEQ: defb 13, 10, "AT+UART_DEF=9600,8,1,0,2", 13, 10
RESET_SEQ_LEN   equ $ - RESET_SEQ

txt_factory_rst:
    defb "! 9600,8,1,n,CTS",13,0    ; FIXME: duplicated strings
    ENDMODULE
; // EOF vim: et:ai:ts=4:sw=4:
