; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

; .uart
    IFNDEF TAP
    DEFINE __ESXDOS_DOT_COMMAND
    ENDIF
    DEFINE OPTION_BAUD_SELECT
    DEFINE OPTION_ESP12_RESET
    ;DEFINE OPTION_UART_115200  ; no implemented properly yet

    DEVICE ZXSPECTRUM48

    IFDEF __ESXDOS_DOT_COMMAND
    org 0x2000
    ELSE
    org 0x8000  ; uncontended!!!
    ENDIF
    IFDEF OPTION_UART_115200
        UNDEFINE OPTION_BAUD_SELECT
    ENDIF

    MACRO SYSCALL adr
    IFDEF __ESXDOS_DOT_COMMAND
    rst 0x18
    dw adr
    ELSE
    call adr
    ENDIF
    ENDM

main:
    ld (EXIT_SP), sp    ; save SP for immediate exit
    IFDEF TAP
    ld hl, 0        ; quick and dirty way to bypass cmd line parser
    ; BASIC needs HL' preserved (see CALCULATE:GEN_ENT_2 at 0x3362)
    ; https://skoolkid.github.io/rom/asm/335B.html
    exx
    ld (EXIT_HL_SHD), hl
    exx
    ENDIF
    ld (SAVE_ARGV), hl  ; save cmd line arguments pointer

    ld hl, text
    call puts       ; print our banner

    ; bit banging works only for specific CPU clock frequencies
    call CPU.cpuspeed
    ;           48k     128k    +3      Scorpion    Pentagon
    ; 3.5/FUSE  4385    4478    4847    5270        5654
    ; 3.5/Sizif 4361    4482    4482    n/a         5632
    ld de, 6000
    or a    ; reset CF, not really necessary (+/- 1 here doesn't matter)
    sbc hl, de
    jr c, cpu_speed_ok
    ld hl, txt_cpu_fast
    call puts
    ld a, 8 ; Access denied
    scf     ; CF=1 -- error, code in A
    ret

cpu_speed_ok:
    ld ix, flags    ; similar to BASIC vars, pointer to settings block
SAVE_ARGV equ $ + 1
    ld hl, 0x0000   ; restore cmd line arguments pointer
    ld a, h
    or l
    jr z, txrx      ; empty cmd line?

    ld de, buffer   ; any input text copied here
    call CMDLINE.PARSE
    bit f_bit_script, (ix+S_FLAGS)
    jr z, .ends
    ld a, 13
    ld (de), a
    inc de
    ld a, 10
    ld (de), a
    inc de
.ends:
    xor a   ; ld a, 0
    ld (de), a  ; null-terminate the copied text

txrx:
    call prnt_settings
    IFDEF OPTION_ESP12_RESET
    bit f_bit_factory_reset, (ix+S_FLAGS)
    jr z, uart_init
    ld hl, ESP12.txt_factory_rst
    call puts
    call ESP12.factory_reset
uart_init:
    ENDIF
    bit f_bit_skip_ay_init, (ix+S_FLAGS)
    jr nz, uart_ready
    ld hl, ay_init_nocr
    call puts
    ld a, (ix+S_AYPRT)
    call UART.init_port
    ld a, (ix+S_RTS)
    call UART.dtr   ; Speccy's pecularity, they use "DTR" where they mean "RTS"
    ld hl, done
    call puts
uart_ready:
    ld hl, txt_brk
    call puts
    ld a, '>'       ; all setup done, tell user about it
    rst 0x10
    ld hl, buffer   ; if we have something to send, send it
txrxloop:
    ld a, (hl)
    or a
    jr z, next
    call UART.RS232_WR_BT
    inc hl
    jr txrxloop
next:
    ld hl, buffer
    call puts

    ld a, 13
    rst 0x10

    bit f_bit_script, (ix+S_FLAGS)
    jr nz, exit

receive_loop:
    call UART.RS232_RD_BT
    jr nc, poll_kbd ; nothing from RS-232, check if we have something to send
    cp 13
    jr z, putc      ; CR -- print it out
    cp 0x20
    jr c, poll_kbd  ; skip anything else before 32
    ; there is no point in printing anything above 127
    cp 0x80     ; for debugging and %0111 1111 may be an option
    jr c, putc
    and 0x7f
    cp 0x20
    jr c, poll_kbd
putc:
    rst 0x10
poll_kbd:
    IFDEF __ESXDOS_DOT_COMMAND
    ; we need to poll keyboard explicitly because esxdos has a simple stub for RST 0x38
    SYSCALL 0x02bf  ; KEYBOARD - https://skoolkid.github.io/rom/asm/02BF.html
    ENDIF
    bit 5, (iy+0x01) ; FLAGS, 5 - Set when a new key has been pressed
    jr z, nokey
    ld a, (0x5c08)
    res 5, (iy+0x01) ; tell interrupt handler that we've got the key
uppercase:
    jr uppercase_int    ; patched to "jr getc_done" if -p supplied
uppercase_int:
    cp 'a'
    jr c, getc_done    
    cp 'z'+1
    jr nc, getc_done
    and %11011111    ; uppercase
getc_done:
    ld c, a
    bit f_bit_echo, (ix+S_FLAGS)
    jr z, uart_write
    rst 0x10        ; echo print
    ld a, c
uart_write:
    call UART.RS232_WR_BT
    ld a, c
    cp 13
    jr nz, nokey
    ld a, 10    ; send LF after CR
    call UART.RS232_WR_BT
    ld (iy+0x52), 255   ; SCR-CT https://skoolkid.github.io/rom/asm/5C8C.html
nokey:
    SYSCALL 0x1f54  ; BREAK-KEY https://skoolkid.github.io/rom/asm/1F54.html
    ; CF=0 if BREAK pressed
    jr c, receive_loop

exit:
    IFDEF TAP   ; restore HL' for BASIC
    exx
EXIT_HL_SHD equ $ + 1
    ld hl, 0
    exx
    ENDIF;TAP
EXIT_SP equ $ + 1
    ld sp, 0x0000    ; cleanup stack, useful for exiting OPTIONS parser
    ld hl, txt_exit
    call puts
    and a   ; CF=0 -- success
    ret

puts: ; HL - NULL terminated string; A, HL not preserved
    ld a, (hl)
    or a
    ret z
    rst 0x10
    inc hl
    jr puts

    IFDEF OPTION_UART_115200
    include "uart-ay-hs.asm"; Baze+Zilog,Martin1 ; 115200 NO flow ctrl
    ELSE
    include "uart-ay.asm"   ; 128k ; 9600 max
    ENDIF

    IFDEF OPTION_ESP12_RESET
    ; better have it in a separate utility? the rest is receiver agnostic
    include "esp12-drv-ay.asm"   ; FIXME: re-use this memory, if no reset required which is typically the case
    ENDIF
; TODO: re-schuffle memory layout, one-time subroutines and texts
; can be moved to the end of the segment
prnt_settings:
    ld a, 6 ; TAB
    rst 0x10
    ld a, ':'
    rst 0x10
    ld e, '-'
    ld b, (ix+S_FLAGS)
    ld d, 'F'
    call prnt_flg
    ld d, 'N'
    call prnt_flg
    ld d, 'E'
    call prnt_flg
    ld d, 'R'
    ld b, (ix+S_RTS)
    call prnt_flg
    ld a, ' ' : rst 0x10 : ld a, 'i' : rst 0x10 : ld a, 'o' : rst 0x10
    ld d, 'A'
    ld e, 'B'
    ld b,(ix+S_AYPRT)
    call prnt_flg
    ld a, ' ' : rst 0x10
    push ix
    ld ix, baud_rates-1
    ld a, (UART.BAUD)
    ld hl, 600
.nb:add hl, hl
    inc ix
    cp (ix+0)
    jr nz, .nb
    pop ix
    call prnt_num
    ld a, 13
    jp 0x10 ; rst $10 will return to our caller
prnt_flg:   ; print D if rightmost bit (0) is set in B, otherwise print E
; A not preserved, B - rotated right
    ld a, d
    rr b
    jr c, .p
    ld a, e
.p: jp 0x10 ; rst $10 will return to our caller
prnt_num:  ; HL - number to print
    push hl
    pop bc  ; OUT_NUM_1 expects number in BC
    SYSCALL 0x1A1B  ; OUT_NUM_1 - https://skoolkid.github.io/rom/asm/1A1B.html
    ; TODO: can we call OUT_NUM_3 ? setup would be more than 2 bytes, though
    ret

flags:  db 0    ; settings block, IX points here
S_FLAGS     equ flags - flags
f_bit_factory_reset     equ 0
f_bit_skip_ay_init      equ 1
f_bit_echo              equ 2
f_bit_script            equ 4
flag_ayprt: db '1'    ; use AY I/O Port 1 (A) by default
S_AYPRT     equ flag_ayprt - flags
flag_rts:   db 0    ; disable RTS check when sending by default
S_RTS       equ flag_rts - flags

text:       defb ".uart v1.0a ", 127, "2024 TIsland Crew", 13, 0
txt_cpu_fast: defb "CPU clock > 3.5MHz - ABORTING!", 13, 0
ay_init_nocr: defb "AY init...", 0
done:       defb 8, 8, 8, " done.", 13, 0
txt_brk:    defb 6, "BREAK to exit", 13, 0
txt_exit:   defb 13, ".uart done", 13, 0
txt_help:   defb " [-{befnprs}] [text to send]", 13
            defb "-b) IO B", 6, "-e)cho", 13
            defb "-n)o AY init", 6, "-r)ts on", 13
            defb "-p)reserve case", 6, "-s)cript", 13
            defb "-f)actory reset ESP12", 13 ; .uart done starts with \n
            ;              1     |   2         3
            ;     12345678901234567890123456789012
            defb 0
baud_rates: defb  $6E,  $36,  $19,  $0B
                ;1200, 2400, 4800, 9600
; TODO: re-shuffle memory layout, move one-time blocks to the end and let buffe(s) overwrite that code
    include "cmd-line.asm"
FLG_B:
    ld (ix+S_AYPRT), '2'
    ret
FLG_E:
    set f_bit_echo, (ix+S_FLAGS)
    ret
FLG_F:
    set f_bit_factory_reset, (ix+S_FLAGS)
    ret
FLG_H:
    ld hl, txt_help
    call puts
    jp exit ; exit cleans up stack
FLG_N:
    set f_bit_skip_ay_init, (ix+S_FLAGS)
    ret
FLG_R:
    ld (ix+S_RTS), 0xff
    ret
FLG_S:
    set f_bit_script, (ix+S_FLAGS)
    ret
FLG_P:
    // no flag, just patch the code ; FIXME: code below is ugly
    ld a, getc_done - uppercase - 2 ; jump to getc_done from uppercase+2
    ld (uppercase + 1), a
    ret
    IFDEF OPTION_BAUD_SELECT
FLG_B1200:
    ld bc, 0x006E
    jr FLG_SET_BAUD
FLG_B2400:
    ld bc, 0x0036
    jr FLG_SET_BAUD
FLG_B4800:
    ld bc, 0x0019
    jr FLG_SET_BAUD
FLG_B9600:
    ld bc, 0x000B
FLG_SET_BAUD:
    ld (UART.BAUD), bc
    ret
    ENDIF;OPTION_BAUD_SELECT
; NOTE: options must be uppercase!!!
@OPTIONS:   defb 0, 'B', 'E', 'F', 'H', 'N', 'P', 'R', 'S'  ; extra 0 needed to use PV, PV=1 (PE) - option found
    IFDEF OPTION_BAUD_SELECT
            defb '1', '2', '4', '9'
    ENDIF;OPTION_BAUD_SELECT
@OPTIONS_LEN equ $ - OPTIONS
@HANDLERS:  defw 0, FLG_B, FLG_E, FLG_F, FLG_H, FLG_N, FLG_P, FLG_R, FLG_S
    IFDEF OPTION_BAUD_SELECT
            defw FLG_B1200, FLG_B2400, FLG_B4800, FLG_B9600
    ENDIF;OPTION_BAUD_SELECT
    ASSERT OPTIONS_LEN == (($ - HANDLERS)/2)
buffer: defb "+++", 0

    ; we start filling the buffer AFTER we check CPU speed
    ; thus, is it safe to overwrite this area
    include "cpuspeed.asm"

CODE_END    equ $
    IFDEF SW_JUMPTABLE
        ALIGN 256   ; no need to actually allocate memory, just compute aligned address
RS232_115200_JUMPTABLE equ $
    DISPLAY "115200 JUMPTABLE OFST:",/A,RS232_115200_JUMPTABLE-main
    ENDIF ; SW_JUMPTABLE
    DISPLAY "CODE SIZE:",/A,CODE_END-main
    IFDEF __ESXDOS_DOT_COMMAND
        SAVEBIN "UART", main, CODE_END-main
    ELSE
        EMPTYTAP"uart.bin.tap"
        SAVETAP "uart.bin.tap",CODE,"uart.bin",main,CODE_END-main
    ENDIF

; EOF vim: et:ai:ts=4:sw=4:
