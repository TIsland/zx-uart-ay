# zx-uart-ay

Simple dot command for communicating with serial devices attached to
AY-8912 (128/+2/+3/Clones) IO port at 9600 baud. Originally created
for managing ESP-12 based WiFi module for Sizif-512.

This utility assumes ESP-12 is already configured with `AT+UART_DEF=9600,8,1,0,2`. It is also capable of reconfiguring factory reset ESP-12, see `-f`.

Usage: `.uart [-b] [-e] [-f] [-n] [-p] [-r] [-s] [text to send]`

 `-b` -- use AY I/O Port 2 (IOBx), for clones with AY-891**0**

 `-e` -- turns on local **e**cho

 `-f` -- initialise ESP-12 after **f**actory reset (sets ESP12 UART speed to 9600,8,1,n,CTS)

 `-n` -- **n**o AY IO port initialisation (not really useful, mostly for debugging)

 `-p` -- **p**reserve case (by default input converted to uppercase)

 `-r` -- check **R**TS status before sending (nothing will be transmitted if receiver does not manage RTS line!)
 
 `-s` -- "script" mode, sends whatver is supplied on the command line and exit

Also, build-in help does not mention speed selection:

 `-1`, `-2`, `-4`, `-9` -- selects 1200, 2400, 4800 or 9600

Optional `text to send` will be transmitted as soon as IO ports are initialised, by default `.uart` sends `+++`.
