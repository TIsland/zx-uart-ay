; the code is re-used in many projects without attribution,
; but actually matches original 128k ROM0:
;   http://www.matthew-wilson.net/spectrum/rom/128_ROM0.html
; which is (C) Amstrad, published and re-used with their permission
; see all disassembly contributors here:
;   http://www.matthew-wilson.net/spectrum/rom/128_ROM0.pdf
;
; MRF project removed portions of the code. We assume it may
; provide better compatibility with ESP-12, but we re-enabled
; those for now (uppercase). Local labels match MRF codebase.
;
; Code modified to allow AY IO port selection, useful for
; clones with a proper AY-8910 installed.
;
; BTW, it should be possible to use much higher speed, e.g.
; https://cygnus.speccy.cz/popis_zx-spectrum_dg192k_rs232.php

    MODULE UART

init_port: ; choose AY IO Port
; A - port selection: 1 or 2; not preserved; flags not preserved
; actually, only lower bit (0) is analysed, so
; xxxx xxx1 selects port A, xxxx xxx0 selects port B
; thus, the valid inputs are: 1, 2; '1', '2'; 'A', 'B'; 'a', 'b'
    and %00000001   ; leave only bit 0, 1-A, 0-B
    xor %00000001   ; invert bit 0, now 0-A, 1-B
    add 14          ; 14 - A, 15 - B
    ld (AYIOPRT), a
    ;ret            ; fall through to AY port(s) setup

init:   ; based on Paul Farrow's code, see https://cygnus.speccy.cz/download/zx128k_rs232/rs232_paul_farrow_57600_data_sequence_with_cts_flow_control.html
    di

    ld	bc, 0xFFFD  ; AY register select port
    ld	a, 7        ; select AY register 7 (Mixer)
    out	(c), a      ;
    ld	a, b        ; A=FF / set IO port as output (1 on bit 6) and disable all sound channels
    ld	b, $BF      ; BC=BFFD, data port
    out	(c), a      ;

    ld	b, a        ; BC=FFFD / AY register port
    ld	a, (AYIOPRT); select AY register 14 (I/O port)
    out	(c), a      ;
    ld	a, b        ; A=FF
    ld	b, $BF      ; BC=BFFD / data port
    out	(c), a      ; Bit 2: RS232  CTS (out) - 0=Spectrum ready to receive, 1=Busy
    ; QUEST: similar setup with CTS low is used by the MRF project, but does
    ; not seem to be necessary as "receiver" routine sets it low before reading
    ; why would be let sender push bytes to us when we are not listening?
    ; drain sender's buffer? Anyway, we don't need it here, we want to see everything

    ei

    halt

    ret				;

dtr:    ; enable/disable DTR check when sending
; NOTE: this is actually RTS line, IIRC it was a mistake carried on
; from the original Interface 1 docs
; A - flag, A=0 -- DTR disabled, A=0xff -- DTR enabled
    and %01000000   ; Bit 6: RS232  DTR (in)  - 0=Device ready for data,     1=Busy
    ld (DTRFL), a   ; patch the code
    ret

                    ; ADDITION: not a part of ROM 0
WRBT_BRK_CHK:       ; check if BREAK pressed and exit RS232_BT_WR
    ; A not preserved, flags not preserved
    SYSCALL 0x1f54  ; BREAK-KEY https://skoolkid.github.io/rom/asm/1F54.html
    RET C           ; CF=0 if BREAK pressed ; A is not preserved
    POP AF          ; return addfress, discard
    POP AF      ; I ; remove AF from stack, see the second line RS232_WR_BT
    JP WRBTEXIT

; ------------------------
; Write Byte to RS232 Port
; ------------------------
; The timing of the routine is achieved using the timing constant held in system variable BAUD.
; Entry: A holds character to send.
; Exit:  Carry and zero flags reset.
RS232_WR_BT:        ; L08A3 128k ROM 0
    push hl, de, bc ; ADDED BY THE MRF PROJECT
    push af     ; I ; Save the byte to send.    ADDITION: see WRBT_BRK_CHK
    ld c, #fd
    ld d, #ff

    ld e, #bf
    ld b, d    
    ld a, (AYIOPRT) ; LD A, #0E
    out (c), a      ; Select AY register 14 to control the RS232 port.

;L08AF:  CALL L05D6        ; Check the BREAK key, and produce error message if it is being pressed.
;        IN   A,(C)        ; Read status of data register.
;        AND  $40          ; %01000000. Test the DTR line.
;        JR   NZ,L08AF     ; Jump back until device is ready for data.
DTRWAIT:
    CALL WRBT_BRK_CHK ; Check the BREAK key, and exit if pressed
    IN A, (C)       ; Read status of data register.
DTRFL   equ $ + 1   ; ADDITION: put 0 to disable DTR check, 0x40 to enable
    AND $40         ; %01000000. Test the DTR line.
    JR NZ, DTRWAIT  ; Jump back until device is ready for data.

    ld hl, (BAUD)   ; $5B5F. HL=Baud rate timing constant.
    ld de, #0002
    or a
    sbc hl, de
    ex hl, de       ; DE=(BAUD)-2.

    pop af      ; I ; Retrieve the byte to send.
    cpl             ; Invert the bits of the byte (RS232 logic is inverted).
    scf             ; Carry is used to send START BIT.
    ld b, #0b       ; B=Number of bits to send (1 start + 8 data + 2 stop).

    di              ; Disable interrupts to ensure accurate timing.

;Transmit each bit
transmitBit:        ; L08C8
    push bc    ; II ; Save the number of bits to send.
    push af    ; II ; Save the data bits.

    ld a, #fe
    ld h, d
    ld l, e         ; HL=(BAUD)-2.
    ld bc, #bffd    ; AY-3-8912 data register.
    jp nc, transmitOne ; L08DA Branch to transmit a 1 or a 0 (initially sending a 0 for the start bit).
;Transmit a 0
    and #f7         ; Clear the RXD (out) line.
    out (c), a      ; Send out a 0 (high level).
    jr transmitNext
;Transmit a 1
transmitOne:        ; L08DA
    or #08          ; Set the RXD (out) line.
    out (c), a      ; Send out a 1 (low level).
    jr transmitNext ; Jump ahead to continue with next bit.

;Delay the length of a bit
transmitNext:       ; L08E0
    dec hl          ; (6) Delay 26*BAUD cycles.
    ld a, h         ; (4)
    or l            ; (4)
    jr nz, transmitNext ; (12) Jump back until delay is completed.
    
    nop             ; (4) Fine tune timing.
    nop             ; (4)
    nop             ; (4)

    pop af     ; II ; Retrieve the data bits to send.
    pop bc     ; II ; Retrieve the number of bits left to send.
    or a            ; Clear carry flag.
    rra             ; Shift the next bit to send into the carry flag.
    djnz transmitBit; Jump back to send next bit until all bits sent.

    ei              ; Re-enable interrupts.
WRBTEXIT:           ; ADDITION: conditional DTR handling
    pop bc, de, hl  // ADDED BY THE MRF PROJECT
    ret             ; Return with carry and zero flags reset.

; -------------------
; RS232 Input Routine
; -------------------
; Exit: Carry flag set if a byte was read with the byte in A. Carry flag reset upon error.

RS232_RD_BT:        ; L06D8 129k ROM 0
    ld hl, SERFL    ; $5B61. SERFL holds second char that can be received
    ld a, (hl)      ; Is the second-character received flag set?
    and a           ; i.e. have we already received data?
    jr z, startReadByte ; Jump ahead if not.

    ld (hl), 0      ; Otherwise clear the flag
    inc hl          ;
    ld a, (hl)      ; and return the data which we received earlier.
    scf             ; Set carry flag to indicate success
    ret             ;

; -------------------------
; Read Byte from RS232 Port
; -------------------------
; The timing of the routine is achieved using the timing constant held in system variable BAUD.
; Exit: Carry flag set if a byte was read, or reset upon error.
;       A=Byte read in.

startReadByte:      ; L06E5
;L06E5:  CALL L05D6        ; Check the BREAK key, and produce error message if it is being pressed.
    di              ; Ensure interrupts are disabled to achieve accurate timing.

    xor a   ; ADDED BY THE MRF PROJECT

    exx             ;

    ld de, (BAUD)   ; $5B71. Fetch the baud rate timing constant.
    ld hl, (BAUD)   ; $5B71.
    srl h           ;
    rr l            ; HL=BAUD/2. So that will sync to half way point in each bit.

    or a            ; [Redundant byte]          

    ld b, #FA       ; Waiting time for start bit.
    exx             ; Save B.
    ld c, #fd       ;
    ld d, #ff       ;
    ld e, #bf       ;
    ld b, d         ;
    ld a, (AYIOPRT) ; LD A, #0E
    out (c), a      ; Selects register 14, port I/O of AY-3-8912.

    in a, (c)       ; Read the current state of the I/O lines.
    or #f0          ; %11110000. Default all input lines to 1.
    and #fb         ; %11111011. Force CTS line to 0.
    ld b, e         ; B=$BF.
    out (c), a      ; Make CTS (Clear To Send) low to indicate ready to receive.

    ld h, a         ; Store status of other I/O lines.

;Look for the start bit
waitStartBit:       ; L070E
    ld b, d         ;
    in a, (c)       ; Read the input line.
    and #80         ; %10000000. Test TXD (input) line.
    jr z, startBitFound ; Jump if START BIT found.

readTimeOut:        ; L0715
    exx             ; Fetch timeout counter
    dec b           ; and decrement it.
    exx             ; Store it.
    jr nz, waitStartBit ; Continue to wait for start bit if not timed out.

    xor a           ; Reset carry flag to indicate no byte read.
    push af         ; Save the failure flag.
    jr readFinish   ; Timed out waiting for START BIT.

startBitFound:      ; L071E
    in a, (c)       ; Second test of START BIT - it should still be 0.
    and #80         ; Test TXD (input) line.
    jr nz, readTimeOut ; Jump back if it is no longer 0.

    in a, (c)       ; Third test of START BIT - it should still be 0.
    and #80         ; Test TXD (input) line.
    jr nz, readTimeOut ; ; Jump back if it is no longer 0.

;A start bit has been found, so the 8 data bits are now read in.
;As each bit is read in, it is shifted into the msb of A. Bit 7 of A is preloaded with a 1
;to represent the start bit and when this is shifted into the carry flag it signifies that 8
;data bits have been read in.
 
    exx             ;
    ld bc, #fffd    ;
    ld a, #80       ; Preload A with the START BIT. It forms a shift counter used to count
    ex af, af       ; the number of bits to read in.

readTune:           ; L0731
    add hl, de      ; HL=1.5*(BAUD).

    nop             ; (4) Fine tune the following delay.
    nop             ;
    nop             ;
    nop             ;

;BD-DELAY
bdDelay:            ; L0736
    dec hl          ; (6) Delay for 26*BAUD.
    ld a, h         ; (4)
    or l            ; (4)
    jr nz, bdDelay  ; (12) Jump back to until delay completed.

    in a, (c)       ; Read a bit.
    and #80         ; Test TXD (input) line.
    jp z, zeroReceived ; Jump if a 0 received.

;Received one 1
    ex af, af       ; Fetch the bit counter.
    scf             ; Set carry flag to indicate received a 1.
    rra             ; Shift received bit into the byte (C->76543210->C).
    jr c, receivedByte ; ; Jump if START BIT has been shifted out indicating all data bits have been received.

    ex af, af       ; Save the bit counter.
    jp readTune     ; Jump back to read the next bit.

;Received one 0
zeroReceived:       ; L074B
    ex af, af       ; Fetch the bit counter.
    or a            ; Clear carry flag to indicate received a 0.
    RRA             ; Shift received bit into the byte (C->76543210->C).
    jr c, receivedByte ; Jump if START BIT has been shifted out indicating all data bits have been received.

    ex af, af       ; Save the bit counter.
    jp readTune     ; Jump back to read next bit.

;After looping 8 times to read the 8 data bits, the start bit in the bit counter will be shifted out
;and hence A will contain a received byte.

receivedByte:       ; L0754
    scf             ; Signal success.
    push af         ; Push success flag.
    exx

;The success and failure paths converge here

readFinish:         ; L0757
    ld a, h         ;
    or #04          ; A=%1111x1xx. Force CTS line to 1.

    ld b, e         ; B=$BF.
    out (c), a      ; Make CTS (Clear To Send) high to indicate not ready to receive.    
    exx

    ld h, d         ;
    ld l, e         ; HL=(BAUD).
    ld bc, #0007    ;
    or a            ;
    sbc hl, bc      ; HL=(BAUD)-7.

delayForStopBit:    ; L0766
    dec hl          ; Delay for the stop bit.
    ld a, h         ;
    or l            ;
    jr nz, delayForStopBit ; Jump back until delay completed.

    ld bc, #fffd    ; HL will be $0000.
    add hl, de      ; DE=(BAUD).
    add hl, de      ;
    add hl, de      ; HL=3*(BAUD). This is how long to wait for the next start bit.

;The device at the other end of the cable may send a second byte even though
;CTS is low. So repeat the procedure to read another byte.

waitStartBitSecondByte: ; L0771
    in a, (c)       ; Read the input line.
    and #80         ; %10000000. Test TXD (input) line.
    jr z, secondStartBitFound ; Jump if START BIT found.

    dec hl          ; Decrement timeout counter.
    ld a, h         ;
    or l            ;
    jr nz, waitStartBitSecondByte ; Jump back looping for a start bit until a timeout occurs.

;No second byte incoming so return status of the first byte read attempt

    pop af          ; Return status of first byte read attempt - carry flag reset for no byte received or
    ei              ; carry flag set and A holds the received byte.
    ret

secondStartBitFound:; L077F
    in a, (c)       ; Second test of START BIT - it should still be 0.
    and #80         ; Test TXD (input) line.
    jr nz, waitStartBitSecondByte ; Jump back if it is no longer 0.

   IN   A,(C)        ; Third test of START BIT - it should still be 0.
   AND  $80          ; Test TXD (input) line.
   JR   NZ,waitStartBitSecondByte     ; L0771 Jump back if it is no longer 0.

;A second byte is on its way and is received exactly as before

    ld h, d         ;
    ld l, e         ; HL=(BAUD).
    ld bc, #0002    ;
    srl h           ;
    rr l            ;
    or a            ;
    sbc hl, bc      ; HL=(BAUD)/2 - 2.

    ld bc, #fffd    ;
    ld a, #80       ; Preload A with the START BIT. It forms a shift counter used to count
    ex af, af       ; the number of bits to read in.

secondByteTune:     ; L079D
    nop             ; Fine tune the following delay.
    nop             ;
    nop             ;
    nop             ;

    add hl, de      ; HL=1.5*(BAUD).

secondDelay:        ; L07A2
    dec hl          ; Delay for 26*(BAUD).
    ld a, h         ;
    or l            ;
    jr nz, secondDelay ; Jump back to until delay completed.

    in a, (c)       ; Read a bit.
    and #80         ; Test TXD (input) line.
    jr z, secondZeroReceived ; Jump if a 0 received.

;Received one 1

    ex af, af       ; Fetch the bit counter.
    scf             ; Set carry flag to indicate received a 1.
    rra             ; Shift received bit into the byte (C->76543210->C).
    jr c, secondByteFinished ; Jump if START BIT has been shifted out indicating all data bits have been received.

    ex af, af       ; Save the bit counter.
    jp secondByteTune ; Jump back to read the next bit.

;Received one 0

secondZeroReceived: ; L07B7
    ex af, af       ; Fetch the bit counter.
    or a            ; Clear carry flag to indicate received a 0.
    rra             ; Shift received bit into the byte (C->76543210->C).
    jr c, secondByteFinished ; Jump if START BIT has been shifted out indicating all data bits have been received.

    ex af, af       ; Save the bit counter.
    jp secondByteTune ; Jump back to read next bit.

;Exit with the byte that was read in

secondByteFinished: ; L07C0
    ld hl, SERFL    ; sysvar SERFL, originally at $5B61
    ld (hl), 1      ; Set the flag indicating a second byte is in the buffer.
    inc hl          ;
    ld (hl), a      ; Store the second byte read in the buffer.
    pop af          ; Return the first byte.

    ei              ; Re-enable interrupts.
    ret

; BAUD sysvar, originally located at $5B5F
; BAUD        EQU $5B5F  ;  2   Baud rate timing constant for RS232 socket. Default value of 11. [Name clash with ZX Interface 1 system variable at $5CC3]

BAUD    dw $0B; 9600, see "Baud Rate Table" below
; SERFL sysvar, originally located at $5B61
; SERFL       EQU $5B61  ;  2   Second character received flag:
;                        ;        Bit 0   : 1=Character in buffer.
;                        ;        Bits 1-7: Not used (always hold 0).
SERFL   dw #0
; NON-STANDARD ADDITION: AY I/O port selection
; classic 128k has AY-8912 which has single I/O port only, however
; AY-8910 has two I/O ports (14 and 15). This is useful for clones
; having proper AY-8910 installed.
@AYIOPRT db 14       ; 14 - IO Port A, 15 - IO Port B
; NON-STANDARD ADDITION: DTR control (CTS doesn't harm)
; unused, code patched directy DTRSFL   db  0       ; 0 - disabled, not 0 - enabled

; Complete BAUD lookup table from 128k ROM 0 (speed, constant)
; ---------------
; Baud Rate Table
; ---------------
; Consists of entries of baud rate value followed by timing constant to use in the RS232 routines.
; 
; L06B8:  DEFW $0032, $0AA5  ; Baud=50.
;         DEFW $006E, $04D4  ; Baud=110.
;         DEFW $012C, $01C3  ; Baud=300.
;         DEFW $0258, $00E0  ; Baud=600.
;         DEFW $04B0, $006E  ; Baud=1200.
;         DEFW $0960, $0036  ; Baud=2400.
;         DEFW $12C0, $0019  ; Baud=4800.
;         DEFW $2580, $000B  ; Baud=9600.

; -------------------------
; AY-3-8912 Sound Generator
; -------------------------
; The AY-3-8912 sound generator is controlled by two I/O ports:
; $FFFD (Out)    - Select a register 0-14.
; $FFFD (In)     - Read from the selected register.
; $BFFD (In/Out) - Write to the selected register. The status of the register can also be read back.
;
; The AY-3-8912 I/O port A is used to drive the RS232 and Keypad sockets.
;
; Register       Function                        Range
; --------       --------                        -----
;; <SKIP>
; 7              Mixer                           8-bit (see end of file for description)
;; <SKIP>
; 14             I/O port A                      8-bit (0-255)
;
; See the end of this document for description on the sound generator registers.
;
; ----------------------------------
; I/O Port A (AY-3-8912 Register 14)
; ----------------------------------
; This controls the RS232 and Keypad sockets.
; Select the port via a write to port $FFFD with 14, then read via port $FFFD and write via port $BFFD. The state of port $BFFD can also be read back.
;
; Bit 0: KEYPAD CTS (out) - 0=Spectrum ready to receive, 1=Busy
; Bit 1: KEYPAD RXD (out) - 0=Transmit high bit,         1=Transmit low bit
; Bit 2: RS232  CTS (out) - 0=Spectrum ready to receive, 1=Busy
; Bit 3: RS232  RXD (out) - 0=Transmit high bit,         1=Transmit low bit
; Bit 4: KEYPAD DTR (in)  - 0=Keypad ready for data,     1=Busy
; Bit 5: KEYPAD TXD (in)  - 0=Receive high bit,          1=Receive low bit
; Bit 6: RS232  DTR (in)  - 0=Device ready for data,     1=Busy
; Bit 7: RS232  TXD (in)  - 0=Receive high bit,          1=Receive low bit
;
; See the end of this document for the pinouts for the RS232 and KEYPAD sockets.
;
; ----------
; Register 7 (Mixer - I/O Enable)
; ----------
; This controls the enable status of the noise and tone mixers for the three channels,
; and also controls the I/O port used to drive the RS232 and Keypad sockets.
;
; Bit 0: Channel A Tone Enable (0=enabled).
; Bit 1: Channel B Tone Enable (0=enabled).
; Bit 2: Channel C Tone Enable (0=enabled).
; Bit 3: Channel A Noise Enable (0=enabled).
; Bit 4: Channel B Noise Enable (0=enabled).
; Bit 5: Channel C Noise Enable (0=enabled).
; Bit 6: I/O Port Enable (0=input, 1=output).
; Bit 7: Not used.
;
; <PINOUT DIGRAM OMITTED>
    ENDMODULE
; EOF vim: et:ai:ts=4:sw=4:
