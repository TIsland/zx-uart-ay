# Copyright 2024 TIsland Crew
# SPDX-License-Identifier: Apache-2.0

SRCS = uart-ay.asm  cmd-line.asm  cpuspeed.asm  uart.asm  esp12-drv-ay.asm
ifdef DEBUG
OPTS = --sym=uart.sym
endif
ifndef VERSION
ifeq ($(MAKECMDGOALS),dist)
$(error VERSION is not defined!)
endif
endif

UART: $(SRCS)
	sjasmplus $(OPTS) uart.asm

uart.tap: loader.tap uart.bin.tap
	cat loader.tap uart.bin.tap > uart.tap
	
uart.bin.tap: $(SRCS)
	sjasmplus $(OPTS) -DTAP uart.asm

%.tap: %.bas
	zmakebas -a 90 -n UART -o $@ $^

dist: UART
	pandoc -t html -s README.md -o README.html
	zip -r9 uart-$(VERSION).zip UART README.html

# EOF vim: noet:ai:ts=8:sw=8:
