; Copyright 2024 TIsland Crew
; SPDX-License-Identifier: Apache-2.0

; 
    MODULE CPU

    ; NOTE: this will fail hard and exit to BASIC if WORKSPACE
    ; does not have measure_end - measure_start (15) bytes
cpuspeed:
    ld bc, measure_end - measure_start
    push bc     ; 1) save allocated area size for RECLAIM_2
    ; BC - Number of free locations to create
    SYSCALL 0x30    ; bc_spaces
    ; DE    Address of the first byte of new free space
    ; HL    Address of the last byte of new free space
    push de     ; 2) save allocated start ptr for RECLAIM_2
    ld (jmpadr), de
    ld hl, measure_start
    ld bc, measure_end - measure_start
    ldir    ; move measuring code to the allocated area
jmpadr equ $ + 1
    SYSCALL 0x000   ; we need to page out esxdos and page it in when we're back
    ; reclaim workspace allocation
    pop hl      ; 2) allocated area start ptr
    pop bc      ; 1) get allocated area size
    push de     ; a) save measured speed
    ; RECLAIM_2 - https://skoolkid.github.io/rom/asm/19E5.html
    ; BC - number of bytes that are to be reclaimed
    ; HL - Start address of the area reclaimed
    SYSCALL 0x19e8
    ; 
    pop hl      ; a) get measured speed in HL
    ret
    ; CPU speed measuring routine, copied to the main RAM
    ; actually, only the part after EI needs to be there
measure_start:
    ld de, 0        ; reset counter
    ld hl, 0x5c78   ; sys FRAMES counter, updated 50 times per sec
    ei  ; FIXME: use z88dk push EI state / pop EI state routines
    halt            ; wait for an interrupt, updating FRAMES
    ld a, (hl)      ; get current FRAMES value (the lowest of 3 bytes)
    inc a           ; next (expected) FRAMES value
wait:   ; count number of iterations in 20ms TODO: maybe use longer loop?
    inc de          ; count number of loops elapsed
    cp (hl)         ; compare with the expected value
    ;res 5, (iy+0x01); 23 t-states, flags are preserved
    jr nc, wait     ; FRAMES not updated yet?
    ret
measure_end:

    DISPLAY "WORKSPACE REQ:", measure_end - measure_start
    ENDMODULE
; EOF vim: et:ai:ts=4:sw=4: